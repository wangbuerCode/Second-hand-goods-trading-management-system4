package model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseOrder<M extends BaseOrder<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public M setGoodName(java.lang.String goodName) {
		set("goodName", goodName);
		return (M)this;
	}
	
	public java.lang.String getGoodName() {
		return getStr("goodName");
	}

	public M setSeller(java.lang.String seller) {
		set("seller", seller);
		return (M)this;
	}
	
	public java.lang.String getSeller() {
		return getStr("seller");
	}

	public M setSellerId(java.lang.Integer sellerId) {
		set("sellerId", sellerId);
		return (M)this;
	}
	
	public java.lang.Integer getSellerId() {
		return getInt("sellerId");
	}

	public M setCustomer(java.lang.String customer) {
		set("customer", customer);
		return (M)this;
	}
	
	public java.lang.String getCustomer() {
		return getStr("customer");
	}

	public M setCustomerId(java.lang.Integer customerId) {
		set("customerId", customerId);
		return (M)this;
	}
	
	public java.lang.Integer getCustomerId() {
		return getInt("customerId");
	}

	public M setGoodId(java.lang.Integer goodId) {
		set("goodId", goodId);
		return (M)this;
	}
	
	public java.lang.Integer getGoodId() {
		return getInt("goodId");
	}

	public M setMoney(java.lang.Integer money) {
		set("money", money);
		return (M)this;
	}
	
	public java.lang.Integer getMoney() {
		return getInt("money");
	}

	public M setSubmitTime(java.util.Date submitTime) {
		set("submitTime", submitTime);
		return (M)this;
	}
	
	public java.util.Date getSubmitTime() {
		return get("submitTime");
	}

	public M setEndTime(java.util.Date endTime) {
		set("endTime", endTime);
		return (M)this;
	}
	
	public java.util.Date getEndTime() {
		return get("endTime");
	}

	public M setStatusId(java.lang.Integer statusId) {
		set("statusId", statusId);
		return (M)this;
	}
	
	public java.lang.Integer getStatusId() {
		return getInt("statusId");
	}

}
