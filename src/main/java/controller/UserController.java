package controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.upload.UploadFile;
import interceptor.Login;
import model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.List;

@Before(Login.class)
public class UserController extends Controller {

    public  void DeleteOrder(){
        Integer orderId = getParaToInt(0, -1);
        Db.delete("delete from `order` where id=?",orderId);
        redirect("/user/userOrder");
    }

    public void userOrder(){
        Student user = getSessionAttr("user");
        List<Order> orders = Order.dao.find("select * from `order` where `order`.customerId=?", user.getStuId());
        setAttr("orders",orders);
        renderFreeMarker("userOrder.ftl");
    }

    public void modifyGoodStatusForZero(){
        Integer goodId = getParaToInt(0, -1);
        Db.update("update good set status=0 where id=?",goodId);
        redirect("/user/userGood");
    }

    public void modifyGoodStatusForOne(){
        Integer goodId = getParaToInt(0, -1);
        Db.update("update good set status=1 where id=?",goodId);
        redirect("/user/userGood");
    }

    public void  DeleteGood(){
        Integer goodId = getParaToInt(0, -1);
        Db.delete("delete from good where id=? ",goodId);
        redirect("/user/userGood");
    }

    public void userGood(){
        Student user = getSessionAttr("user");
        List<Good> goods = Good.dao.find("select good.*,type.type from good,type where userId=? and good.typeId=type.id", user.getStuId());
        setAttr("goods",goods);
        renderFreeMarker("userGood.ftl");

    }

    public void DomodifiyPersonInfo(){
        Integer stuId = getParaToInt("stuId");
        String stuName = getPara("stuName");
        String password = getPara("password");
        String major = getPara("major");
        String QQ = getPara("QQ");
        String email = getPara("email");
        String gender = getPara("gender");
        String tel = getPara("tel");
        Db.update("update student set stuName=?,password=?,major=?,tel=?,QQ=?,gender=?,email=? where stuId=?",stuName,password,major,tel,QQ,gender,email,stuId);
        Boolean success = true;
        String message = success ? "更新成功" : "更新失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void modifyPersonInfo(){
        Student student = getSessionAttr("user");
        setAttr("student",student);
        renderFreeMarker("modifyPersonInfo.ftl");

    }
    public void uerInfo(){
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find("select * from student where stuId=? ", student.getStuId());
//        List<Collection> collections = Collection.dao.find("SELECT * from collection,good where collection.goodId=good.id and collection.userId=?", student.getStuId());
        setAttr("student",students.get(0));
//        setAttr("collection",collections);

        SqlPara sqlPara = Db.getSqlPara("uerInfo",student.getStuId());
        Integer pageNumber = getParaToInt("page", 1);
        Page<Collection> page = Collection.dao.paginate(pageNumber, 6, sqlPara);
        setAttr("page", page);

        renderFreeMarker("uerInfo.ftl");
    }

    public void finishOrder(){
        Integer orderId = getParaToInt(0, -1);
        Date date = new Date();
        Db.update("update `order` set statusId =1,endTime=? where id =?",date,orderId);
        List<Order> orders = Order.dao.find("select * from `order` where id=?", orderId);

        setAttr("id",orders.get(0).getId());
        setAttr("order",orders.get(0));
        renderFreeMarker("finishOrder.ftl");
    }

    public void buyGood(){
        Integer goodId = getParaToInt(0, -1);
        Integer stuId = getParaToInt(1, -1);
        List<Good> goods = Good.dao.find("select * from good,student where good.userId=student.stuId and good.id=?", goodId);
        List<Student> students = Student.dao.find("select * from student where stuId=?", stuId);

        Order order = new Order();
        Date date = new Date();
        order.setCustomer(students.get(0).getStuName());
        order.setCustomerId(students.get(0).getStuId());
        order.setGoodName(goods.get(0).getGoodName());
        order.setSeller(goods.get(0).get("stuName"));
        order.setSellerId(goods.get(0).get("stuId"));
        order.setMoney(goods.get(0).getPrice());
        order.setSubmitTime(date);
        order.setStatusId(0);
        order.setGoodId(goodId);
        order.save();
        List<Record> records = Db.find("SELECT max(id) as id from `order`");
        setAttr("id",records.get(0).get("id"));
        setAttr("order",order);
        renderFreeMarker("buyGood.ftl");


    }

    public void reply(){
        Integer stuId = getParaToInt("stuId");
        Integer goodId = getParaToInt("goodId");
        String content = getPara("content");
        Reply reply = new Reply();
        Date date = new Date();
        reply.setPubDate(date);
        reply.setUserId(stuId);
        reply.setContent(content);
        reply.setGoodId(goodId);
        reply.save();
        Boolean success = true;
        String message = success ? "回复成功" : "回复失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void deleteReply(){
        Integer replyId = getParaToInt(0, -1);
        Integer goodId = getParaToInt(1, -1);
        Db.delete("delete from reply where id=?",replyId);

        redirect("/goodDetail/"+goodId);

    }

    public void DoModifyGoodInfo(){
        String goodName = getPara("goodName");
        Integer price = getParaToInt("goodPrice");
        String goodDetail = getPara("goodDetail");
        Integer type = getParaToInt("goodType");
        Integer status = getParaToInt("status");
        Integer goodId = getParaToInt("goodId");

        Student user = getSessionAttr("user");
        Integer stuId = user.getStuId();
        Date date = new Date();
        Db.update("update good set goodName=?,price=?,goodDetail=?,typeId=?,status=?,publishTime=? where id=?",goodName,price,goodDetail,type,status,date,goodId);

        Boolean success = true;
        String message = success ? "请继续上传图片" : "修改失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void modifyGood(){
        Integer goodId = getParaToInt(0, -1);

        List<Good> goods = Good.dao.find("select good.*,student.stuName,type.type,student.tel,student.qq from type,good,student where good.id=? and good.userId=stuId and good.typeId=type.id ", goodId);
        setAttr("good",goods.get(0));
        renderFreeMarker("modifyGood.ftl");

        List<Type> types = Type.dao.find("SELECT * from type");
        setAttr("types", types);

    }


    public void CancelCollectGood(){
        Integer goodId = getParaToInt(0, -1);
        Integer userId = getParaToInt(1, -1);
        Db.delete("delete from collection where goodId=? AND userId=? ",goodId,userId);

        redirect("/goodDetail/"+goodId);
    }

    public void DoCollectGood() {
        Integer goodId = getParaToInt(0, -1);
        Integer userId = getParaToInt(1, -1);
        Collection collection = new Collection();
        Date date = new Date();
        collection.setUserId(userId);
        collection.setGoodId(goodId);
        collection.setCollectTime(date);
        collection.save();

        redirect("/goodDetail/"+goodId);
    }

    public void DoUploadPic() {

        Boolean success = false;
        UploadFile upload = this.getFile();
        String fileName = upload.getOriginalFileName();

        File file = upload.getFile();
        String contentType = upload.getContentType();


        String webRootPath = PathKit.getWebRootPath();//得到web路径

        PropKit.use("myconfig.properties");//从配置文件中读取保存路径
//            String saveFilePathforimage = PropKit.get("saveFilePathforimage");
        String saveFilePathforimage = webRootPath + "\\template\\image\\";
        String filename = file.getName();
        String savaFileName = filename;

        System.out.println("保存路径=" + saveFilePathforimage);

        String saveNme = saveFilePathforimage + savaFileName;

        String mysql_save_Path = "/template/image/" + savaFileName;


        File Direction = new File(saveFilePathforimage);
        //判断文件夹是否存在 如果不存在 就创建文件夹
        if (!Direction.exists()) {
            Direction.mkdirs();
        }

        File t = new File(saveNme);
        try {
            Student user = getSessionAttr("user");
            System.out.println("当前的StuID=" + user.getStuId());
            List<Good> goods = Good.dao.find("select * from good  WHERE userId=? ORDER BY publishTime desc  LIMIT 1", user.getStuId());
            System.out.println("当前的goodID=" + goods.get(0).getId());

            t.createNewFile();
            System.out.println("测试点1");
            if (count == 1) {
                System.out.println("处于第一个");
                int update = Db.update("update good set imageUrlOne = ? where id =?", mysql_save_Path, goods.get(0).getId());
                System.out.println("处于第一个的结果" + update);
            } else if (count == 2) {
                System.out.println("处于第2个");
                Db.update("update good set imageUrlTwo = ? where id =?", mysql_save_Path, goods.get(0).getId());
            } else if (count == 3) {
                System.out.println("处于第3个");
                Db.update("update good set imageUrlThree = ? where id =?", mysql_save_Path, goods.get(0).getId());

            }


            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            LogKit.error("上传失败，原因是：" + e.getMessage());
        }
        fileChannelCopy(file, t);
        file.delete();

        String message = success ? "上传成功" : "上传失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void fileChannelCopy(File s, File t) {
        FileInputStream fi = null;
        FileOutputStream fo = null;
        FileChannel in = null;
        FileChannel out = null;
        try {
            fi = new FileInputStream(s);
            fo = new FileOutputStream(t);
            in = fi.getChannel();//得到对应的文件通道
            out = fo.getChannel();
            in.transferTo(0, in.size(), out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fi.close();
                in.close();
                fo.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static int count = 0;

    public void uploadImage() {
        count++;
        System.out.println("当前的count=" + count);

        Student user = getSessionAttr("user");
        List<Good> goods = Good.dao.find("select * from good  WHERE userId=? ORDER BY publishTime desc  LIMIT 1", user.getStuId());
        setAttr("good", goods.get(0));
        setAttr("count", count);

        if(count>3) {
            count = 0;
        }
        renderFreeMarker("uploadImage.ftl");


    }

    public void SaveGoodInfo() {

        String goodName = getPara("goodName");
        Integer price = getParaToInt("goodPrice");
        String goodDetail = getPara("goodDetail");
        Integer type = getParaToInt("goodType");
        Integer status = getParaToInt("status");

        Student user = getSessionAttr("user");
        Integer stuId = user.getStuId();
        Date date = new Date();

        Good good = new Good();
        good.setGoodName(goodName);
        good.setGoodDetail(goodDetail);
        good.setTypeId(type);
        good.setPrice(price);
        good.setPublishTime(date);
        good.setStatus(status);
        good.setUserId(user.getStuId());
        good.setImageUrlOne("null");
        good.setImageUrlTwo("null");
        good.setImageUrlThree("null");
        good.save();
        Boolean success = true;
        String message = success ? "发布成功" : "发布失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void release() {
        List<Type> types = Type.dao.find("SELECT * from type");
        setAttr("types", types);
        renderFreeMarker("realease.ftl");
    }

    public void index() {
        renderHtml("user页面");
    }

    public void logout() {
        removeSessionAttr("user");
        redirect("/login");
    }
}
